/* jshint esversion:6, newcap:false, camelcase:false */
import 'google-apps-script';

//stage
// var CLIENT_ID = '683660006843-tqt6v2u665e4jav43g9rl5646nprf03g.apps.googleusercontent.com';
// var CLIENT_SECRET = 'BzQ0FH8qgCBKWb9FZZnO9IFL';
// var HOST_URL = 'https://stage.hippowiz.com/';
// var EXTENSION_URL = 'https://chrome.google.com/webstore/detail/hippo-video-screen-video/dnebjjdbjiikcmihpkcanphpbibhofkh';
// var HOST = 'stage_';

// production
var CLIENT_ID = '683660006843-cugaf52uto0rs0u943k29dl8g4gqpgg0.apps.googleusercontent.com';
var CLIENT_SECRET = 'anCbnD2IAmT_1bRy-43tXXd9'; 
var HOST_URL = 'https://www.hippovideo.io/';
var EXTENSION_URL = 'https://chrome.google.com/webstore/detail/hippo-video-screen-video/cijidiollmnkegoghpfobabpecdkeiah';
var HOST = 'prod_';

var SERVICE_NAME = "GMAIL";
var AUTH_URL = 'https://accounts.google.com/o/oauth2/auth';
var TOKEN_URL = 'https://accounts.google.com/o/oauth2/token';
var SERVICE_SCOPE_REQUESTS = 'https://www.googleapis.com/auth/userinfo.email '+
                             'https://www.googleapis.com/auth/userinfo.profile ';
                             
var AUTH_TOKEN_CACHE_KEY = 'HIPPO_AUTH_TOKEN';
var REDIRECT_TO_ERROR_PAGE = 'error_page_redirection';
var REDIRECT_TO_SIGN_IN_PAGE = 'sign_in_page_redirection';
var SHOW_PLAN_TYPE = 'show_plan_type';
var ASK_COMPANY_DETAILS = 'ask_company_details';
var IS_NEW_USER = 'is_new_user';

var COMPOSE_ICON = HOST_URL + 'images/compose_icon.png';
var REPLY_ALL_ICON = HOST_URL + 'images/reply_all_icon.png';
var REPLY_ICON = HOST_URL + 'images/reply_icon.png';
var REPORTS_ICON = HOST_URL + 'images/reports_icon.png';

function getOAuthService() {
  //get oauth service for hippo video
  return OAuth2.createService(SERVICE_NAME)
      .setAuthorizationBaseUrl(AUTH_URL)
      .setTokenUrl(TOKEN_URL)
      .setClientId(CLIENT_ID)
      .setClientSecret(CLIENT_SECRET)
      .setScope(SERVICE_SCOPE_REQUESTS)
      .setCallbackFunction('signInUser')
      .setCache(CacheService.getUserCache())
      .setParam('login_hint', Session.getEffectiveUser().getEmail())
      .setPropertyStore(PropertiesService.getUserProperties());
}

function authorize(event) {
    var userProperties = PropertiesService.getUserProperties();
    var hippoAuthToken = userProperties.getProperty(HOST+AUTH_TOKEN_CACHE_KEY);
    if(hippoAuthToken == null) {
        var service = getOAuthService();
        logToGasLogs('authorize', 'hippoAuthToken is null');
        CardService.newAuthorizationException().setAuthorizationUrl(service.getAuthorizationUrl()).setCustomUiCallback('signInUser').setResourceDisplayName('Hippo Video').throwException();
    } 
}

function signInUser(event) {
     logToGasLogs('signInUser', 'entered into signin user');
     var accessToken = ScriptApp.getOAuthToken();
      var email = Session.getEffectiveUser().getEmail() ;
      var url = HOST_URL + 'api/chrome_signup.json';
      var headers = {'Accept': 'application/json'};
      var data = {refresh_token: accessToken, email: email, request_type: 'api', referer: 'gmail-add-on'};
      var options = {'headers': headers,'method': 'post', 'payload': data, 'muteHttpExceptions': true};
      var resp = UrlFetchApp.fetch(url, options);
      var code = resp.getResponseCode();
      var userProperties = PropertiesService.getUserProperties();
      if (code >= 200 && code < 300) {
          var respString = resp.getContentText();
          var responseJson = JSON.parse(respString);
          logToGasLogs('signInUser', 'user signed in successfully');
          userProperties.setProperty(HOST + AUTH_TOKEN_CACHE_KEY, responseJson.auth_token);
          userProperties.setProperty(HOST + SHOW_PLAN_TYPE, responseJson.show_get_started+'');
          userProperties.setProperty(HOST + ASK_COMPANY_DETAILS, responseJson.ask_company_details + '');
          var isNewUser = userProperties.getProperty(HOST + IS_NEW_USER);
          if(isNewUser == null) {
             userProperties.setProperty(HOST + IS_NEW_USER, responseJson.is_new_user + '');
          }
          return [getContextualAddOn(event)];
      }
}

function signOut(event) {
    logToGasLogs('signOut', 'user signed out successfully');
    var userProperties = PropertiesService.getUserProperties();
    userProperties.deleteProperty(HOST + AUTH_TOKEN_CACHE_KEY);
    userProperties.deleteProperty(HOST + IS_NEW_USER);
    var card = signInCard();
    return CardService.newUniversalActionResponseBuilder().displayAddOnCards([card]).build();
}

function signInCard() {
    logToGasLogs('signInCard', 'sign in card requested');
    var action = CardService.newAction().setFunctionName('signInUserThroughSignInCard').setLoadIndicator(CardService.LoadIndicator.SPINNER);
    return CardService.newCardBuilder().setName('signinPage')
        .setHeader(header(' '))
        .addSection(CardService.newCardSection()
            .addWidget(image(HOST_URL + 'images/gmail-gs/signin_img1.png', 'Hippo Video'))
            .addWidget(image(HOST_URL + 'images/gmail-gs/signin_img2.jpg', 'SIGN IN').setOnClickAction(action))
            .addWidget(image(HOST_URL + 'images/gmail-gs/signin_img3.jpg')))
        .build(); 
}

function signInUserThroughSignInCard(event) {
    var cardArray = signInUser(event);
    if(cardArray) {
        return cardArray[0];
    } else {
        return errorPage();
    }
}

//triggers every time add-on is clicked
function getContextualAddOn(event) {     
    logToGasLogs('getContextualAddOn', 'entered');
    var userProperties = PropertiesService.getUserProperties();      
    var hippoAuthToken = userProperties.getProperty(HOST+ AUTH_TOKEN_CACHE_KEY);
    
    var isNewUser = userProperties.getProperty(HOST + IS_NEW_USER);
    if(isNewUser == 'true') {
        logToGasLogs('getContextualAddOn', 'returned full version home page');
        return fullVersionHomePage();
    }

    var showPlanTypeSelection = userProperties.getProperty(HOST + SHOW_PLAN_TYPE);
    var askCompanyDetails = userProperties.getProperty(HOST + ASK_COMPANY_DETAILS);
    if (hippoAuthToken == null || showPlanTypeSelection == null || askCompanyDetails == null) {
        logToGasLogs('getContextualAddOn', 'hippoAuthToken is null');
        signInUser(event);
    }

    if(showPlanTypeSelection == 'true') {
        logToGasLogs('getContextualAddOn', 'returned plan type card');
        return getChoosePlanTypeCard().build();
    } else if(askCompanyDetails == 'true') {
        logToGasLogs('getContextualAddOn', 'returned domain form card');
        return showDomainForm({}).build();
    } else {
        var getStartedShown = userProperties.getProperty(HOST + 'show_get_started');
        if (getStartedShown === 'true') {
            logToGasLogs('getContextualAddOn', 'returned home page');
            return showHomePage(event);
        } else {
            logToGasLogs('getContextualAddOn', 'returned tour card');
            userProperties.setProperty(HOST + 'show_get_started', 'false');
            var tourCard = takeTour();
            return tourCard;
        }
    }
}

function fullVersionHomePage() {
    var section = CardService.newCardSection();
    section.addWidget(image(HOST_URL + 'images/gmail_add_on/hippo-logo.png', 'Hippo Video').setOnClickOpenLinkAction(action('openHippoVideoHome')));
    section.addWidget(textParagraph('Welcome to Hippo Video! We highly recommend installing our chrome extension to get the best out of videos and Gmail.'));
    section.addWidget(iconTextAndBottomLabel(HOST_URL + 'img/active-state-green.png', 'Record expert videos', 'Capture screen, webcam, and audio'));
    section.addWidget(iconTextAndBottomLabel(HOST_URL + 'img/active-state-green.png', 'Leverage the power of video emails', 'Personalized campaigns, 1:1 personal videos'));
    section.addWidget(iconTextAndBottomLabel(HOST_URL + 'img/active-state-green.png', 'Track video emails', 'Video performance, customer insights'));
    section.addWidget(image(HOST_URL + 'images/gmail_add_on/install-full-version.png', 'Install Full Version').setOnClickOpenLinkAction(action('openFullVersionLink')));
    section.addWidget(image(HOST_URL + 'images/gmail_add_on/chrome-extension.jpg', 'Install Hippo Video Chrome Extension'));
    section.addWidget(textParagraph('Click on the button below, if you\'d like to continue with the basic Gmail Addon'));
    section.addWidget(image(HOST_URL + 'images/gmail_add_on/hippo-video-gmail-addon.png').setOnClickAction(action('proceedToAddOn')));
    return CardService.newCardBuilder().addSection(section).build();
}

function showFullVersionCard() {
    var section = CardService.newCardSection();
    var card = CardService.newCardBuilder();
    card.setHeader(header('Get the Full Version of Hippo Video', 'Record, Track Emails and more', HOST_URL + 'images/hippo-video-logo-48px.png'));
    section.addWidget(textParagraph('To ensure you have the best experience of Hippo Video in Gmail, We highly recommend installing our chrome extension.'));
    section.addWidget(iconTextAndBottomLabel(HOST_URL + 'img/active-state-green.png', 'Record expert videos', 'Capture screen, webcam, and audio'));
    section.addWidget(iconTextAndBottomLabel(HOST_URL + 'img/active-state-green.png', 'Leverage the power of video emails', 'Personalized campaigns, 1:1 personal videos'));
    section.addWidget(iconTextAndBottomLabel(HOST_URL + 'img/active-state-green.png', 'Track video emails', 'Video performance, customer insights'));
    section.addWidget(image(HOST_URL + 'images/gmail_add_on/install-full-version.png', 'Install Full Version').setOnClickOpenLinkAction(action('openFullVersionLink')));
    section.addWidget(image(HOST_URL + 'images/gmail_add_on/chrome-extension.jpg', 'Install Hippo Video Chrome Extension'));
    return CardService.newActionResponseBuilder().setNavigation(CardService.newNavigation().pushCard(card.addSection(section).build())).build();
}

function openHippoVideoHome(event) {
    var hippovideoHomeLink = openLink(HOST_URL, CardService.OpenAs.FULL_SIZE, CardService.OnClose.NOTHING);
    return CardService.newActionResponseBuilder().setOpenLink(hippovideoHomeLink).build();
}

function openFullVersionLink(event) {
    logToGasLogs('openFullVersionLink', 'clicked full version link');
    var userProperties = PropertiesService.getUserProperties();
    userProperties.setProperty(HOST + IS_NEW_USER, 'false');
    var fullVersionPageLink = openLink(HOST_URL + '/videorecorder-screenrecorder-chrome-extension.html', CardService.OpenAs.FULL_SIZE, CardService.OnClose.NOTHING);
    var card = getContextualAddOn(event);
    return CardService.newActionResponseBuilder().setOpenLink(fullVersionPageLink).setNavigation(CardService.newNavigation().updateCard(card)).build();
}

function proceedToAddOn(event) {
    logToGasLogs('proceedToAddOn', 'clicked continue add on');
    var userProperties = PropertiesService.getUserProperties();
    userProperties.setProperty(HOST + IS_NEW_USER, 'false');
    var card = getContextualAddOn(event);
    return CardService.newActionResponseBuilder().setNavigation(CardService.newNavigation().updateCard(card)).build();
}

function showHomePage(event) {
      var homeCard = getHomeCard(event);
      if (homeCard.state == 'custom_ui') {
          logToGasLogs('showHomePage', 'custom ui error - 1');
          signInUser(event);
          homeCard = getHomeCard(event);
          if (homeCard.state == 'custom_ui') {
              logToGasLogs('showHomePage', 'custom ui error - 2');
             return errorCard().build();
          }
      } else if (homeCard.state == 'error') {
          logToGasLogs('showHomePage', 'error case - 1');
          signInUser(event);
          homeCard = getHomeCard(event);
          if (homeCard.state == 'custom_ui') {
              logToGasLogs('showHomePage', 'error case - 2');
              return errorCard().build();
          }
      } 
      return homeCard.card.build();
}

function getHomeCard(event) {
    var threadId;
    if(event) {
        var accessToken = event.messageMetadata.accessToken;
        GmailApp.setCurrentMessageAccessToken(accessToken);
        var messageId = event.messageMetadata.messageId;
        var message = GmailApp.getMessageById(messageId);
        threadId = message.getThread().getId();
    }
    
    var cache = CacheService.getUserCache();
    cache.remove(HOST+REDIRECT_TO_SIGN_IN_PAGE);
    cache.remove(HOST+REDIRECT_TO_ERROR_PAGE);

    var userProperties = PropertiesService.getUserProperties();
    var hippoAuthToken = userProperties.getProperty(HOST + AUTH_TOKEN_CACHE_KEY);
    if (hippoAuthToken == undefined || hippoAuthToken == null || hippoAuthToken === '') {
        logToGasLogs('getHomeCard', 'hippoAuthToken is null');
        signInUser(event);
    }

    var showGetStarted = userProperties.getProperty(HOST + SHOW_PLAN_TYPE);
    var askCompanyDetails = userProperties.getProperty(HOST + ASK_COMPANY_DETAILS);
    var card;
    if(showGetStarted == 'true') {
        logToGasLogs('getHomeCard', 'returned plan type card');
        return { state: 'success', card: getChoosePlanTypeCard()};
    } else if (askCompanyDetails == 'true') {
        logToGasLogs('getHomeCard', 'returned domain form card');
        return { state: 'success', card: showDomainForm({})};
    } else {
        card = CardService.newCardBuilder().setName('homePage')
                          .setHeader(header('Menu'));
                            
        var recentActivitiesInfo = getRecentActivities(threadId);
        var showFullVersion = (recentActivitiesInfo.sections == null) || (recentActivitiesInfo.sections.length == 0);
        if(!showFullVersion) {
            for (var index = 0; index < recentActivitiesInfo.sections.length; index++) {
                var recentActivitiesSection = recentActivitiesInfo.sections[index];
                card.addSection(recentActivitiesSection);
            }
        }

        var createVideosSection = getCreateVideosSection(threadId);
        card.addSection(createVideosSection);
        var guestLinkSection = getGuestLinkSection();
        if (guestLinkSection != null) {
            card.addSection(guestLinkSection);
        }
        if(showFullVersion) {
            card.addSection(getFullVersionSection());
        }
        card.addSection(dummySection());
    
        var redirectToSignInPage = cache.get(HOST+REDIRECT_TO_SIGN_IN_PAGE);
        var redirectToErrorPage = cache.get(HOST+REDIRECT_TO_ERROR_PAGE);
        if(redirectToSignInPage != null && redirectToSignInPage) {
            cache.remove(HOST+REDIRECT_TO_SIGN_IN_PAGE);
            return {'state': 'custom_ui'};
        } else if(redirectToErrorPage != null && redirectToErrorPage) {
            cache.put(HOST+REDIRECT_TO_ERROR_PAGE, false);
            return {'state': 'error'};
        } else {
            return {'state': 'success', 'card': card};
        } 
    }
}

function getFullVersionSection() {
    var section = CardService.newCardSection();
    section.addWidget(iconTextAndBottomLabel(HOST_URL + 'images/hippo-video-logo-48px.png', 'Get the Full Version of Hippo Video', 'Record, Track Emails and more')
                     .setOnClickAction(action('showFullVersionCard')));
    return section;
}

function errorPage() {
    return errorCard().build();
}

function errorCard() {
    return CardService.newCardBuilder().setHeader(header('  '))
                          .addSection(CardService.newCardSection()
                          .addWidget(textParagraph('Sorry, something went wrong')));
}


function getRecentActivities(threadId) {
    var toRet = {sections: null, msg: ''};
    var userProperties = PropertiesService.getUserProperties();
    var hippoAuthToken = userProperties.getProperty(HOST+AUTH_TOKEN_CACHE_KEY);
    var cache = CacheService.getUserCache();
    if(hippoAuthToken == undefined || hippoAuthToken == null || hippoAuthToken === '') {
        //auth token is not present in cache
        cache.put(HOST+REDIRECT_TO_SIGN_IN_PAGE, true);
        return toRet;
    } else {
        if(threadId) {
            var url = HOST_URL + 'utm/reports/gmail.json';
            var email = Session.getEffectiveUser().getEmail();
            url += "?email=" + email + '&authentication_token=' + hippoAuthToken + '&legacy_thread_id=' + threadId + '&request_type=api';
            var headers = {
                'Accept': 'application/json'
            };
            var options = {
                'headers': headers,
                'method': 'get',
                'muteHttpExceptions': true
            };
            var response = UrlFetchApp.fetch(url, options);
            var code = response.getResponseCode();
            var section;
            var responseJson;
            if (code >= 200 && code < 300) {
                var respString = response.getContentText();
                responseJson = JSON.parse(respString);
                var sections = [];
                if (responseJson.status) {
                    if (responseJson.has_videos) {
                        var assets = responseJson.asset_stats;
                        for (var index = 0; index < assets.length; index++) {
                            section = CardService.newCardSection();
                            var asset = assets[index];
                            var hasVideoInfo = asset.has_video_info;
                            if (hasVideoInfo) {
                                if (index == 0) {
                                    section.addWidget(textParagraph('<b>STATS</b>'));
                                }
                                var previewUrl = HOST_URL + 'video/view/' + asset.asset_id;
                                section.addWidget(iconTextAndBottomLabel(asset.thumbnail_url, asset.title, asset.duration + ' | ' + asset.created_at).setOpenLink(openLink(previewUrl, CardService.OpenAs.FULL_SIZE)));
                                if (asset.mail_opened) {
                                    var hasPlayStats = asset.has_play_stats;
                                    if (hasPlayStats) {
                                        section = constructPlayStats(section, asset);
                                    } else {
                                        section.addWidget(textParagraph('<font color="#919191">Recipients have opened your email. They are yet to click on the video.</font>'));
                                    }
                                } else {
                                    section.addWidget(textParagraph('<font color="#919191">Recipients have received your email. They are yet to open the mail.</font>'));
                                }
                                sections.push(section);
                            } 
                        }
                    } 
                }
                if (sections.length == 0) {
                    toRet.sections = [];
                    toRet.msg = 'showFullVersion';
                    return toRet;
                } else {
                    toRet.sections = sections;
                    return toRet;
                }
            } else {
                if (responseJson.err_msg === 'Authentication Error') {
                    userProperties.deleteProperty(HOST + AUTH_TOKEN_CACHE_KEY);
                }
                if (isUserSignedIn(hippoAuthToken)) {
                    cache.put(HOST + REDIRECT_TO_ERROR_PAGE, true);
                } else {
                    cache.put(HOST + REDIRECT_TO_SIGN_IN_PAGE, true);
                }
                return toRet;
            }
        } else {
            toRet.sections = [];
            return toRet;
        }
    }
}

function constructPlayStats(section, asset) {
    if (parseInt(asset.views) == 0) {
        section.addWidget(textParagraph('<font color="#919191">Recipients have opened your email. They are yet to click on the video.</font>'));
    } else {
        var times = asset.views == 1 ? ' time ' : ' times ';
        var users = asset.unique_views == 1 ? ' recipient ' : ' recipients ';
        var has = asset.unique_views == 1 ? 'has' : 'have';
        section.addWidget(textParagraph('<font color="#4ea7bc"><b>' + asset.unique_views + '</b><font><font color="#616161"><i>' + users + '</i></font><font color="#919191">' + has + ' </font><font color="#616161"><i>viewed</i></font><font color="#919191"> your video </font><font color="#4ea7bc"><b>' + asset.views + '</b></font><font color="#616161"><i>' + times + '</i></font>' ));
        if (asset.plays == 0) {
            section.addWidget(textParagraph('<font color="#919191">Recipients have clicked on your video. They are yet to play it.</font>'));
        } else {
            var viewers;
            times = asset.plays == 1 ? ' time ' : ' times ';
            users = asset.unique_plays == 1 ? ' recipient ' : ' recipients ';
            has = asset.unique_views == 1 ? 'has' : 'have';
            section.addWidget(textParagraph('<font color="#4ea7bc"><b>' + asset.unique_plays + '</b><font><font color="#616161"><i>' + users + '</i></font><font color="#919191">' + has + ' </font><font color="#616161"><i>played</i></font><font color="#919191"> your video </font><font color="#4ea7bc"><b>' + asset.plays + '</b></font><font color="#616161"><i>' + times + '</i></font>' ));
            if (parseInt(asset.plays) != 0) {
                section.addWidget(textParagraph('<b>Watch Rate<b>'));
                viewers = asset.less_than_25 == 1? ' view' : ' views';
                section.addWidget(keyValue('0% - 25%', '<font color="#4ea7bc"><b>' + asset.less_than_25 + '</b></font><font color="#616161"><i>' + viewers + '</i></font>'));
                viewers = asset.watch_rate_25 == 1? ' view' : ' views';
                section.addWidget(keyValue('25% - 50%', '<font color="#4ea7bc"><b>' + asset.watch_rate_25 + '</b></font><font color="#616161"><i>' + viewers + '</i></font>'));
                viewers = asset.watch_rate_50 == 1? ' view' : ' views';
                section.addWidget(keyValue('50% - 100%', '<font color="#4ea7bc"><b>' + asset.watch_rate_50 + '</b></font><font color="#616161"><i>' + viewers + '</i></font>'));
                viewers = asset.watch_rate_100 == 1? ' view' : ' views';
                section.addWidget(keyValue('100%', '<font color="#4ea7bc"><b>' + asset.watch_rate_100 + '</b></font><font color="#616161"><i>' + viewers + '</i></font>'));
            }
        }
    }
    return section;
}

function dummySection() {
    return CardService.newCardSection().addWidget(textParagraph('  '));
}

function getCreateVideosSection(threadId) {
    var id = '';
    if(threadId) {
        id = threadId;
    }
    var email = Session.getEffectiveUser().getEmail();
    var userProperties = PropertiesService.getUserProperties();
    var hippoAuthToken = userProperties.getProperty(HOST + AUTH_TOKEN_CACHE_KEY);
    var section = CardService.newCardSection();
    section = section.addWidget(textParagraph('<b>CREATE YOUR VIDEO</b>'));
    section = section.addWidget(iconAndText(HOST_URL + 'images/gmail_add_on_intro_video_icon.png', 'Create Intro / Thank You Video').setOpenLink(openLink(HOST_URL + 'video/record?email='+ email +'&authentication_token='+ hippoAuthToken +'&tag=intro&initiator=gmail&id='+id, CardService.OpenAs.FULL_SIZE)));
    section = section.addWidget(iconAndText(HOST_URL + 'images/gmail_add_on/explainer-video.png', 'Create Explainer Video').setOpenLink(openLink(HOST_URL + 'video/record?email=' + email + '&authentication_token=' + hippoAuthToken + '&initiator=gmail&id=' + id, CardService.OpenAs.FULL_SIZE)));
    section = section.addWidget(iconAndText(HOST_URL + 'images/gmail_add_on/record-video.png', 'Record Video').setOpenLink(openLink(HOST_URL + 'video/record?email=' + email + '&authentication_token=' + hippoAuthToken + '&initiator=gmail&id=' + id, CardService.OpenAs.FULL_SIZE)));
    section = section.addWidget(iconAndText(HOST_URL + 'images/gmail_add_on/import-video.png', 'Import Video').setOpenLink(openLink(HOST_URL + 'video/record?email=' + email + '&authentication_token=' + hippoAuthToken + '&tag=import', CardService.OpenAs.FULL_SIZE)));
    var params = {'startOffset': '0', 'page': '0'};
    section = section.addWidget(iconAndText(HOST_URL + 'images/gmail_add_on/library.png', 'Add Video from Library').setOnClickAction(action('showVideos', params, CardService.LoadIndicator.SPINNER)));
    return section;
}

function getGuestLinkSection() {
    var grEnabled = isGuestRecordingEnabled();
    if(grEnabled) {
        var section = CardService.newCardSection();
        section = section.addWidget(CardService.newButtonSet()
                         .addButton(imageButton( HOST_URL + 'images/gmail_add_on_guest_link_icon.png', 'Seek a video reply').setOnClickAction(action('emptyFunction')))
                         .addButton(textButton('<font color="#212121">SEEK VIDEO REPLY</font>').setOnClickAction(action('emptyFunction')))
                         .addButton(imageButton(COMPOSE_ICON, 'Compose').setComposeAction(action('attachGuestLink',{'from': 'compose'}, CardService.LoadIndicator.SPINNER), CardService.ComposedEmailType.STANDALONE_DRAFT))
                         .addButton(imageButton(REPLY_ICON, 'Reply').setComposeAction(action('attachGuestLink',{'from': 'reply'}, CardService.LoadIndicator.SPINNER), CardService.ComposedEmailType.REPLY_AS_DRAFT))
                         .addButton(imageButton(REPLY_ALL_ICON, 'Reply all').setComposeAction(action('attachGuestLink',{'from': 'reply_all'}, CardService.LoadIndicator.SPINNER), CardService.ComposedEmailType.REPLY_AS_DRAFT)));
        return section;
    } 
    return null;
}

function emptyFunction() {
}

function composeReply(event) {
    var accessToken = event.messageMetadata.accessToken;
    GmailApp.setCurrentMessageAccessToken(accessToken);
    var threadId = event.threadId;
    var msgId = event.messageMetadata.messageId;
    var params = event.parameters;
    
    var thumbnailUrl = HOST_URL + 'video/thumbnail/' + params.token;
    var shareUrl = params.url;
    var utmTokenResponse = getUTMToken(params.token, threadId);
    var utmToken = utmTokenResponse.utm_token;
    var addPixel = false;
    if (!utmTokenResponse.thumbnail_personalised) {
        thumbnailUrl = HOST_URL + 'video/thumbnail/gmail/' + params.token;
        addPixel= true;
    }  
    if(utmToken) {
        shareUrl += '?utm_source=gmail&utm_content=' + utmToken;
        thumbnailUrl += '?utm_source=gmail&utm_content=' + utmToken;
    } 
    
    var html;
    if(addPixel) {
        html = '<div id="hippo-video-compose-box-' + params.token + '" class="hippo-video"><a href="' + shareUrl + '" target="_blank" rel="no_opener"><img style="width: 100%; max-width: 360px;" src="' + utmTokenResponse.thumbnail_url + '"/></a><img src="' + thumbnailUrl + '" /></div>';
    } else { 
        html = '<div id="hippo-video-compose-box-' + params.token + '" class="hippo-video"><a href="' + shareUrl + '" target="_blank" rel="no_opener"><img style="width: 100%; max-width: 360px;" src="' + thumbnailUrl + '"/></a></div>';
    }
    
    var message = GmailApp.getMessageById(msgId);
    var draft;
    var action = params.from;
    if (action == 'reply') {
        draft = message.createDraftReply('',{ htmlBody: html });
        logToHippoVideo('Click:Reply:' + params.asset_id);
    } else if(action == 'reply_all') {
        draft = message.createDraftReplyAll('',{ htmlBody: html });
        logToHippoVideo('Click:Reply_All:' + params.asset_id);
    }
    
    return CardService.newComposeActionResponseBuilder().setGmailDraft(draft).build();
}

function showStats(event) {

    var accessToken = event.messageMetadata.accessToken;
    GmailApp.setCurrentMessageAccessToken(accessToken);

    var params = event.parameters;
    var assetId = params.asset_id;
    var thumbnail = params.thumbnail;
    var title = params.title;
    var duration = params.duration;
    var createdAt = params.created_at;
    
    logToHippoVideo('Click:Video_Stats:'+assetId);
    
    var card = CardService.newCardBuilder().setHeader(header(title));
    
    var videoSection = CardService.newCardSection();
    videoSection = videoSection.addWidget(textParagraph('<font color="#919191">' + duration + ' | ' + createdAt + '</font>'));
    videoSection = videoSection.addWidget(image(thumbnail, title));
    card = card.addSection(videoSection);
    
    var statsSection = getStats(assetId);
    card = card.addSection(statsSection);
    
    card = card.addSection(dummySection());
   
    var nav = CardService.newNavigation().pushCard(card.build());
    return CardService.newActionResponseBuilder().setNavigation(nav).build();
}

function showVideos(event) {

    var accessToken = event.messageMetadata.accessToken;
    GmailApp.setCurrentMessageAccessToken(accessToken);
    
    logToHippoVideo('Click:Show_More');
    
    var params = event.parameters;
    var startOffset = params.startOffset;
    var numVideos = params.num_videos;
    
    var videosJson = getVideos(startOffset);
    var videos = videosJson.videos;
    if(startOffset == '0') {
        numVideos = videosJson.num_videos;
    }

    var card, section;
    if(videos.length == 0) {
        card = CardService.newCardBuilder().setHeader(header('My Videos'));
        section = CardService.newCardSection();
        section.addWidget(textParagraph('No Videos Found'));
    } else {
        card = CardService.newCardBuilder().setHeader(header('My Videos (' + (parseInt(startOffset) + 1) + '-' + (parseInt(startOffset) + videos.length) + ')'));
        section = CardService.newCardSection();
        for (var index = 0; index < videos.length; index++) {
            section = addVideoWidget(section, videos[index]);
        }
        var page = parseInt(params.page);
        var videosLoaded = (page * 20) + videos.length;
        var totalVideos = parseInt(numVideos);
        if(videosLoaded < totalVideos) {
            params = {'startOffset': videosLoaded.toString(), 'num_videos': numVideos.toString(), 'page': (page+1).toString()};
            section.addWidget(CardService.newButtonSet()
                            .addButton(imageButton(HOST_URL + 'images/gmail_add_on_white_space.jpg', ' ').setOnClickAction(action('emptyFunction')))
                            .addButton(imageButton(HOST_URL + 'images/gmail_add_on_white_space.jpg', ' ').setOnClickAction(action('emptyFunction')))
                            .addButton(textButton('SHOW MORE').setOnClickAction(action('showVideos',params , CardService.LoadIndicator.SPINNER))));
        }
    }
    card = card.addSection(section).build();
    
    var cache = CacheService.getUserCache();
    var redirectToSignInPage = cache.get(HOST+HOST+REDIRECT_TO_SIGN_IN_PAGE);
    var redirectToErrorPage = cache.get(HOST+REDIRECT_TO_ERROR_PAGE);
    if(redirectToSignInPage != null && redirectToSignInPage) {
        cache.remove(HOST+REDIRECT_TO_SIGN_IN_PAGE);
        signInUser(event);
        card = errorPage();
    } else if(redirectToErrorPage != null && redirectToErrorPage) {
        cache.put(HOST+REDIRECT_TO_ERROR_PAGE, false);
        card = errorPage();
    } 
    var nav = CardService.newNavigation().pushCard(card);
    return CardService.newActionResponseBuilder().setNavigation(nav).build();
}


//Utils

function isGuestRecordingEnabled() {
    var userProperties = PropertiesService.getUserProperties();
    var hippoAuthToken = userProperties.getProperty(HOST+AUTH_TOKEN_CACHE_KEY);
    if(hippoAuthToken == undefined || hippoAuthToken == null || hippoAuthToken === '') {
        //auth token is not present in cache
        var cache = CacheService.getUserCache();
        cache.put(HOST+REDIRECT_TO_SIGN_IN_PAGE, true);
        return false;
    }
    var url = HOST_URL + 'api/user/token_valid.json';
    var email = Session.getEffectiveUser().getEmail();
    var headers = {'Accept': 'application/json'};
    var params = {email: email, authentication_token: hippoAuthToken, request_type: 'api'};
    var options = {'headers': headers,'method': 'post', 'muteHttpExceptions': true, payload: params};
    var response = UrlFetchApp.fetch(url, options);
    var code = response.getResponseCode();
    if (code >= 200 && code < 300) {
      var respString = response.getContentText();
      var responseJson = JSON.parse(respString);
      if (responseJson.err_msg) {
          if (responseJson.err_msg  === 'Authentication Error') {
            userProperties.deleteProperty(HOST+AUTH_TOKEN_CACHE_KEY);
          } 
      } else {
          var features = responseJson.feature_params;
          if(features != null) {
              var guestRecording = features.guest_recording;
              if(guestRecording) {
                  return true;
              }
          }
      }
    }
    return false;
}


function attachGuestLink(event) {

    logToHippoVideo('Click:GuestLink');
    
    var accessToken = event.messageMetadata.accessToken;
    GmailApp.setCurrentMessageAccessToken(accessToken);
    var params = event.parameters;
    var guestLink = getGuestLink();
    if(guestLink) {
        var html = '<label>Please use this link <a href="'+ guestLink +'">'+ guestLink +'</a> to reply with a video. You don\'t have to sign up.</label>';
        var from = params.from;
        var draft;
        var messageId = event.messageMetadata.messageId;
        var message = GmailApp.getMessageById(messageId);
        if(from == 'compose') {
            draft = GmailApp.createDraft('','','', { htmlBody: html });
        } else if (from == 'reply') {
            draft = message.createDraftReply('',{ htmlBody: html });
        } else if(from == 'reply_all') {
            draft = message.createDraftReplyAll('',{ htmlBody: html });
        }
        return CardService.newComposeActionResponseBuilder().setGmailDraft(draft).build();
    }
}

function getGuestLink() {
    var userProperties = PropertiesService.getUserProperties();
    var hippoAuthToken = userProperties.getProperty(HOST+AUTH_TOKEN_CACHE_KEY);
    if(hippoAuthToken == undefined || hippoAuthToken == null || hippoAuthToken === '') {
        //auth token is not present in cache
        var cache = CacheService.getUserCache();
        cache.put(HOST+REDIRECT_TO_SIGN_IN_PAGE, true);
        return null;
    }
    var url = HOST_URL + 'api/user/guest_link.json'
    var email = Session.getEffectiveUser().getEmail();
    url += "?email=" + email +'&authentication_token='+hippoAuthToken;
    var headers = {'Accept': 'application/json'};
    var options = {'headers': headers,'method': 'post', 'muteHttpExceptions': true, 'payload': {request_type: 'api'}};
    var response = UrlFetchApp.fetch(url, options);
    var code = response.getResponseCode();
    if (code >= 200 && code < 300) {
      var respString = response.getContentText();
      var responseJson = JSON.parse(respString);
      if(responseJson.err_msg) {
          if (responseJson.err_msg  === 'Authentication Error') {
            userProperties.deleteProperty(HOST+AUTH_TOKEN_CACHE_KEY);
          } 
      } else {
          return responseJson.url;
      }
    } 
    return null;
}


function addVideoWidget(section, video) {
   if(video.duration == null) {
       video.duration = '00m:00s';
   }
  if(video.thumb == null || !video.thumb) {
    if(video.thumbnail_url != null && video.thumbnail_url) {
        video.thumb = video.thumbnail_url;
    } else {
        video.thumb = HOST_URL + '/images/bg-hippo-one.png';
    }
  }
  var token = video.url.split('/');
  token = token[token.length-1];
  token = token.split('?')[0];
  var previewUrl = HOST_URL + 'video/view/' + video.id;
  var paramsForReply = {'from': 'reply', 'url': video.url, 'thumbnail': video.thumb, 'token': token, 'asset_id': video.id.toString()};
  var paramsForReplyAll = {'from': 'reply_all', 'url': video.url, 'thumbnail': video.thumb, 'token': token, 'asset_id': video.id.toString()};
  var paramsForStats = {'asset_id': video.id.toString(), 'thumbnail': video.thumb, 'title': video.title, 'duration': video.duration.toString(), 'created_at': video.created_at};
  section.addWidget(iconTextAndBottomLabel(video.thumb, video.title, video.duration + ' | ' + video.created_at).setOpenLink(openLink(previewUrl, CardService.OpenAs.FULL_SIZE)));
  section.addWidget(CardService.newButtonSet()
                    .addButton(imageButton(HOST_URL + 'images/gmail_add_on_white_space.jpg', '').setOnClickAction(action('emptyFunction')))
                    .addButton(imageButton(REPLY_ICON, 'Reply').setComposeAction(action('composeReply',paramsForReply, CardService.LoadIndicator.SPINNER), CardService.ComposedEmailType.REPLY_AS_DRAFT))
                    .addButton(imageButton(REPLY_ALL_ICON, 'Reply all').setComposeAction(action('composeReply',paramsForReplyAll, CardService.LoadIndicator.SPINNER), CardService.ComposedEmailType.REPLY_AS_DRAFT))
                    .addButton(imageButton(REPORTS_ICON, 'Stats').setOnClickAction(action('showStats',paramsForStats , CardService.LoadIndicator.SPINNER))));

   return section;
}


function getUTMToken(token, threadId) {
    var userProperties = PropertiesService.getUserProperties();
    var hippoAuthToken = userProperties.getProperty(HOST+AUTH_TOKEN_CACHE_KEY);
    if(hippoAuthToken == undefined || hippoAuthToken == null || hippoAuthToken === '') {
        //auth token is not present in cache
        var cache = CacheService.getUserCache();
        cache.put(HOST+REDIRECT_TO_SIGN_IN_PAGE, true);
        return null;
    }
    var url = HOST_URL + 'video/utm/gmail/' + token + '.json';
    var email = Session.getEffectiveUser().getEmail();
    url += '?email=' + email + '&authentication_token=' + hippoAuthToken + '&legacy_thread_id=' + threadId + '&from=add-on&request_type=api';
    var headers = {'Accept': 'application/json'};
    var options = {'headers': headers,'method': 'get', 'muteHttpExceptions': true};
    var response = UrlFetchApp.fetch(url, options);
    var code = response.getResponseCode();
    if (code >= 200 && code < 300) {
      var respString = response.getContentText();
      var responseJson = JSON.parse(respString);
       if(responseJson.err_msg) {
           if (responseJson.err_msg  === 'Authentication Error') {
              userProperties.deleteProperty(HOST+AUTH_TOKEN_CACHE_KEY);
            } 
        } else {
          return responseJson;
        }
    } 
    return null;
}

function getStats(assetId) {
    var userProperties = PropertiesService.getUserProperties();
    var hippoAuthToken = userProperties.getProperty(HOST+AUTH_TOKEN_CACHE_KEY);
    var section = CardService.newCardSection();
    if(hippoAuthToken) {
        var url = HOST_URL + 'utm/reports/gmail/'+assetId+ '.json';
        var email = Session.getEffectiveUser().getEmail();
        url += '?email=' + email + '&authentication_token=' + hippoAuthToken + '&request_type=api';
        var headers = {'Accept': 'application/json'};
        var options = {'headers': headers,'method': 'get', 'muteHttpExceptions': true};
        var response = UrlFetchApp.fetch(url, options);
        var code = response.getResponseCode();
        if (code >= 200 && code < 300) {
            var respString = response.getContentText();
            var responseJson = JSON.parse(respString);
            if(responseJson.status) {
                if (responseJson.has_messages && responseJson.no_of_messages > 0) {
                    var emails = responseJson.no_of_messages == 1 ? ' email' : ' emails';
                    section = section.addWidget(textParagraph('<font color="#919191">You attached this video in </font><font color="#4ea7bc"><b>' + responseJson.no_of_messages + '</b></font><font color="#616161"><i>' + emails + '</i></font>'));
                    emails = responseJson.no_of_messages_opened == 1 ? ' email' : ' emails';
                    var were = responseJson.no_of_messages_opened == 1 ? ' was ' : ' were ';
                    section = section.addWidget(textParagraph('<font color="#4ea7bc"><b>' + responseJson.no_of_messages_opened + '</b></font><font color="#616161"><i>' + emails + '</i></font><font color="#919191">' + were + '</font><font color="#616161"><i>opened</i></font>'));
                    if (responseJson.has_play_stats) {
                        section = constructPlayStats(section, responseJson);
                    } else {
                        if (responseJson.no_of_messages_opened == 0) {
                            section.addWidget(textParagraph('<font color="#919191">Recipients have received your email. They are yet to open the mail.</font>'));
                        } else {
                            section.addWidget(textParagraph('<font color="#919191">Recipients have clicked on your video. They are yet to play it.</font>'));
                        }
                    }
                    return section;
                } else {
                    return section.addWidget(textParagraph('<font color="#919191">You haven\'t attached this video in any email. Start using videos to drive awesome results.</font>'));
                }
            } else {
                if (responseJson.err_msg) {
                    if (responseJson.err_msg  === 'Authentication Error') {
                        userProperties.deleteProperty(HOST+AUTH_TOKEN_CACHE_KEY);
                    } 
                }
            }
        } 
    }
    return section.addWidget(textParagraph('<font color="#919191">Looks like you haven\'t installed the Hippo Video Extension.Please <a href="' + EXTENSION_URL +'">install</a> it, to populate your video performance stats.'));
}


function getVideos(startOffset) {
    var userProperties = PropertiesService.getUserProperties();
    var hippoAuthToken = userProperties.getProperty(HOST+AUTH_TOKEN_CACHE_KEY);
    var cache = CacheService.getUserCache();
    if(hippoAuthToken == undefined || hippoAuthToken == null || hippoAuthToken === '') {
        //auth token is not present in cache
        cache.put(HOST+REDIRECT_TO_SIGN_IN_PAGE, true);
        return null;
    } else {
        var url = HOST_URL + 'video/all_videos/'+startOffset + '.json';
        var email = Session.getEffectiveUser().getEmail();
        url += '?email=' + email + '&authentication_token=' + hippoAuthToken + '&request_type=api';
        var headers = {'Accept': 'application/json'};
        var options = {'headers': headers,'method': 'get', 'muteHttpExceptions': true};
        var response = UrlFetchApp.fetch(url, options);
        var code = response.getResponseCode();
        if (code >= 200 && code < 300) {
            var respString = response.getContentText();
            var responseJson = JSON.parse(respString);
            if (responseJson.err_msg) {
                if (responseJson.err_msg  === 'Authentication Error') {
                  userProperties.deleteProperty(HOST+AUTH_TOKEN_CACHE_KEY);
                } 
                if(isUserSignedIn(hippoAuthToken)) {
                    cache.put(HOST+REDIRECT_TO_ERROR_PAGE, true);
                } else {
                    cache.put(HOST+REDIRECT_TO_SIGN_IN_PAGE, true);
                }
            } else {
                var videos = responseJson.libraries;
                var numVideos = responseJson.num_videos;
                return {'videos': videos, 'num_videos': numVideos};
            }
        } else {
            if(isUserSignedIn(hippoAuthToken)) {
                cache.put(HOST+REDIRECT_TO_ERROR_PAGE, true);
            } else {
                cache.put(HOST+REDIRECT_TO_SIGN_IN_PAGE, true);
            }
        }
    }
    return null;
}

function isUserSignedIn(hippoAuthToken) {
    var email = Session.getEffectiveUser().getEmail();
    var url = HOST_URL + 'video/delivery/user_info';
    var headers = {'Accept': 'application/json'};
    url += "?email=" + email + '&authentication_token=' + hippoAuthToken + '&request_type=api';
    var options = { 'headers': headers, 'method': 'get', 'muteHttpExceptions': true };
    var response = UrlFetchApp.fetch(url, options);
    var code = response.getResponseCode();
    if (code >= 200 && code < 300) {
      var respString = response.getContentText();
      var responseJson = JSON.parse(respString);
      if (responseJson.err_msg  === 'Authentication Error') {
          PropertiesService.getUserProperties().deleteProperty(HOST+AUTH_TOKEN_CACHE_KEY);
          return false;
      } else {
          if (responseJson.user_name == 'Guest' && responseJson.user_email == 'nil') {
              return false;
          } 
      }
    } else {
        return false;
    }
    return true;
}

//gmail add on tour

function takeTourFromMenu() {
    var userProperties = PropertiesService.getUserProperties();
    userProperties.setProperty(HOST+'show_get_started', 'false');
    logToHippoVideo('GmailAddOn:TakeTour:FromMenu');
    var tourCard = takeTour();
    return [tourCard];
}

function takeTour() {
    var userProperties = PropertiesService.getUserProperties();
    var stuckInTour = userProperties.getProperty(HOST+'struck_in_tour');
    if(stuckInTour) {
        return handleTourStruck(stuckInTour);
    } else {
        var card = CardService.newCardBuilder();
        card.setHeader(header('Get Started with Hippo Video'));
        card.addSection(getStartScreen());
        return card.build();
    }
}

function getStartScreen() {
    var startTourAction = action('extensionScreen', {}, CardService.LoadIndicator.SPINNER);
    var skipTourAction = action('skipTour', {}, CardService.LoadIndicator.SPINNER);
    var tourStartScreenSection = CardService.newCardSection()
                                .addWidget(image(HOST_URL + 'images/gmail-gs/gmail_addon_GS1_img1.jpg', 'Take a Tour')) 
                                .addWidget(image(HOST_URL + 'images/gmail-gs/gmail_addon_GS1_img2.jpg', 'Start Tour').setOnClickAction(startTourAction)) 
                                .addWidget(image(HOST_URL + 'images/gmail-gs/gmail_addon_GS1_img3.jpg', 'Skip Tour').setOnClickAction(skipTourAction)); 
    return tourStartScreenSection;
}

function skipTour(event) {
    logToHippoVideo('GmailAddOn:GS:skipTourClicked');
    return endTour(event);
}

function handleTourStruck(screenNo) {
    if(screenNo == 1) {
        return extensionScreen();
    } else if(screenNo == 2) {
        return composeScreen();
    } else if(screenNo == 3) {
        return recordOrChooseScreen();
    } else if(screenNo == 4) {
        return attachAndSendScreen();
    } else if(screenNo == 5) {
        return analyticsScreen();
    }
}

function extensionScreen() {
    logToHippoVideo('GmailAddOn:GS:S1:StartTourClicked');
    var userProperties = PropertiesService.getUserProperties();
    userProperties.setProperty(HOST+'struck_in_tour', 1);
    var composeCard = CardService.newCardBuilder();
    composeCard.setHeader(header('Get Started with Hippo Video'));
    var goNextAction = action('composeScreen', {}, CardService.LoadIndicator.SPINNER);
    var getChromeExtensionAction = action('openExtensionPage');
    composeCard.addSection(CardService.newCardSection()
                                .addWidget(image(HOST_URL + 'images/gmail-gs/gmail_addon_GS_ext_img1.jpg', 'Hippo Video Extension')) 
                                .addWidget(image(HOST_URL + 'images/gmail-gs/gmail_addon_GS_ext_img2.jpg', 'Install Hippo Video Chrome Extension').setOnClickOpenLinkAction(getChromeExtensionAction))
                                .addWidget(image(HOST_URL + 'images/gmail-gs/next.jpg', 'Next').setOnClickAction(goNextAction))
                                .addWidget(image(HOST_URL + 'images/gmail-gs/gmail_addon_GS_ext_img3.jpg', 'Next'))); 
    return composeCard.build();
}

function openExtensionPage(event) {
    logToHippoVideo('GmailAddOn:GS:ExtScr:GetChromeExtensionClicked');
    var userProperties = PropertiesService.getUserProperties();
    userProperties.setProperty(HOST+'struck_in_tour', 2);
    var chromeExtOpenLink = openLink(EXTENSION_URL, CardService.OpenAs.FULL_SIZE, CardService.OnClose.RELOAD_ADD_ON);
    return CardService.newActionResponseBuilder().setOpenLink(chromeExtOpenLink).build();
}


function composeScreen(event) {
    var userProperties = PropertiesService.getUserProperties();
    userProperties.setProperty(HOST+'struck_in_tour', 2);
    var composeCard = CardService.newCardBuilder();
    composeCard.setHeader(header('Get Started with Hippo Video'));
    var composeAction = action('triggerComposeBox', {}, CardService.LoadIndicator.SPINNER);
    var goNextAction = action('recordOrChooseScreen', {}, CardService.LoadIndicator.SPINNER);
    composeCard.addSection(CardService.newCardSection()
                                .addWidget(image(HOST_URL + 'images/gmail-gs/gmail_addon_GS2_img1.jpg', 'Compose')) 
                                .addWidget(image(HOST_URL + 'images/gmail-gs/gmail_addon_GS2_img2.jpg', 'Compose').setComposeAction(composeAction, CardService.ComposedEmailType.STANDALONE_DRAFT)) 
                                .addWidget(image(HOST_URL + 'images/gmail-gs/next2.jpg', 'Next').setOnClickAction(goNextAction))); 
    return composeCard.build();
}

function triggerComposeBox(event) {   
   logToHippoVideo('GmailAddOn:GS:S2:ComposeClicked');
   var accessToken = event.messageMetadata.accessToken;
   GmailApp.setCurrentMessageAccessToken(accessToken);
   var email = Session.getEffectiveUser().getEmail() ;
   
   var token = 'bOyOJiq5MZHtmbfLN2wO0A';
   var thumbnailUrl = 'https://www.hippovideo.io/video/thumbnail/'+token; 
   var shareUrl = 'https://www.hippovideo.io/video/play/'+ token;
   var html = '<div id="hippo-video-compose-box-' + token + '" class="hippo-video"><a href="' + shareUrl + '" target="_blank" rel="no_opener"><img style="width: 100%; max-width: 360px;" src="' + thumbnailUrl + '"/></a></div>';
 
   var draft = GmailApp.createDraft(email,'Start using videos for business','', { htmlBody: html , cc:'hello@hippovideo.io'});
   return CardService.newComposeActionResponseBuilder().setGmailDraft(draft).build();
}

function recordOrChooseScreen(event) {
    var userProperties = PropertiesService.getUserProperties();
    userProperties.setProperty(HOST+'struck_in_tour', 3);
    var recordOrChooseCard = CardService.newCardBuilder();
    recordOrChooseCard.setHeader(header('Get Started with Hippo Video'));
    var NextAction = action('attachAndSendScreen', {}, CardService.LoadIndicator.SPINNER);
    var getChromeExtensionAction = action('openExtensionPage');
    var section = CardService.newCardSection();
    section.addWidget(image(HOST_URL + 'images/gmail-gs/gmail_addon_GS3_img1.jpg', 'Record or Choose'));
    section.addWidget(image(HOST_URL + 'images/gmail-gs/next.jpg', 'Next').setOnClickAction(NextAction));
    recordOrChooseCard.addSection(section);
    return recordOrChooseCard.build();
}

function attachAndSendScreen(event) {
    var userProperties = PropertiesService.getUserProperties();
    userProperties.setProperty(HOST+'struck_in_tour', 4);
    var attachAndSendScreen = CardService.newCardBuilder();
    attachAndSendScreen.setHeader(header('Get Started with Hippo Video'));
    var NextAction = action('analyticsScreen', {}, CardService.LoadIndicator.SPINNER);
    attachAndSendScreen.addSection(CardService.newCardSection()
                                .addWidget(image(HOST_URL + 'images/gmail-gs/gmail_addon_GS4_img1.jpg', 'Attach and Send'))
                                .addWidget(image(HOST_URL + 'images/gmail-gs/next.jpg', 'Next').setOnClickAction(NextAction))); 
    return attachAndSendScreen.build();
}

function analyticsScreen() {
    var userProperties = PropertiesService.getUserProperties();
    userProperties.setProperty(HOST+'struck_in_tour', 5);
    var analyticsCard = CardService.newCardBuilder();
    analyticsCard.setHeader(header('Get Started with Hippo Video'));
    var finishAction = action('finishTour', {}, CardService.LoadIndicator.SPINNER);
    var openPricingPage = action('openPricingPage');
    var section = CardService.newCardSection();
    section.addWidget(image(HOST_URL + 'images/gmail-gs/gmail_addon_GS5_img1.jpg', 'Analytics'));
    section.addWidget(image(HOST_URL + 'images/gmail-gs/gmail_addon_GS5_img3.jpg', 'Analytics works only with Extension'));
    section.addWidget(image(HOST_URL + 'images/gmail-gs/gmail_addon_GS5_img2.jpg', 'Finish').setOnClickAction(finishAction));
    section.addWidget(image(HOST_URL + 'images/gmail-gs/gmail_addon_GS5_img4.jpg', 'Check Pricing').setOnClickOpenLinkAction(openPricingPage));
    analyticsCard.addSection(section); 
    return analyticsCard.build();
}

function openPricingPage(event) {
    logToHippoVideo('GmailAddOn:GS:PricingClicked');
    var pricingPageLink = openLink(HOST_URL+'settings/subscription', CardService.OpenAs.FULL_SIZE, CardService.OnClose.NOTHING);
    return CardService.newActionResponseBuilder().setOpenLink(pricingPageLink).build();
}

function finishTour(event) {
    logToHippoVideo('GmailAddOn:GS:S5:FinishTourClicked');
    return endTour(event);
}

function endTour(event) {
    var userProperties = PropertiesService.getUserProperties();
    userProperties.setProperty(HOST+'show_get_started', 'true');
    userProperties.deleteProperty(HOST+'struck_in_tour');
    return showHomePage(event);
}  

//Logging

function logToHippoVideo(actionName) {
    var userProperties = PropertiesService.getUserProperties();
    var hippoAuthToken = userProperties.getProperty(HOST+AUTH_TOKEN_CACHE_KEY);
    if(hippoAuthToken == undefined || hippoAuthToken == null || hippoAuthToken === '') {
        var cache = CacheService.getUserCache();
        cache.put(HOST+REDIRECT_TO_SIGN_IN_PAGE, true);
        return;
    }
    var email = Session.getEffectiveUser().getEmail();
    var url = HOST_URL + 'user/gmail/track_action';
    var headers = {'Accept': 'application/json'};
    var data = {email: email, request_type: 'api', action_name: actionName, request_type: 'api', authentication_token: hippoAuthToken};
    var options = {'headers': headers,'method': 'post', 'payload': data, 'muteHttpExceptions': true};
    var response = UrlFetchApp.fetch(url, options);
    var code = response.getResponseCode();
    if (code >= 200 && code < 300) {
      var respString = response.getContentText();
      var responseJson = JSON.parse(respString);
      if(responseJson.err_msg  === 'Authentication Error') {
          PropertiesService.getUserProperties().deleteProperty(HOST+AUTH_TOKEN_CACHE_KEY);
      }
    }
}

function getChoosePlanTypeCard() {
    // return CardService.newCardBuilder().setName('planTypeSelectionCard').addSection(CardService.newCardSection()
    // .addWidget(image(HOST_URL+'images/gmail_add_on/plan-type-selection.jpg', ''))
    // .addWidget(image(HOST_URL + 'images/gmail_add_on/plan-type-text.jpg', ''))
    // .addWidget(image(HOST_URL + 'images/gmail_add_on/plan-type-business.jpg', 'Business').setOnClickAction(action('showIndividualSegmentScreen', {plan_type: '4'}, CardService.LoadIndicator.SPINNER)))
    // .addWidget(image(HOST_URL+'images/gmail_add_on/plan-type-personal.jpg', 'Personal').setOnClickAction(action('showIndividualSegmentScreen', {plan_type: '1'}, CardService.LoadIndicator.SPINNER))));
    return showBusinessIndividualSegments();
}

function showIndividualSegmentScreen(event) {
    var params = event.parameters;
    if(params.plan_type == '4') {
        return showBusinessIndividualSegments();
    } else if(params.plan_type == '1') {
        return showPersonalIndividualSegmants();
    }
}

function showBusinessIndividualSegments() {
    var card =  CardService.newCardBuilder().setName('BusinessIndividualSegmentCard')
    .addSection(CardService.newCardSection()
    .addWidget(image(HOST_URL + 'images/gmail_add_on/plan-type-selection.jpg', ''))
    .addWidget(image(HOST_URL + 'images/gmail_add_on/business-ind-seg-text.jpg', ''))
    .addWidget(image(HOST_URL + 'images/gmail_add_on/business-email-campaign.png', 'Send bulk emails with personalized videos at scale').setOnClickAction(action('savePlanType', {plan_type: '4', individual_segment: 'Campaign'}, CardService.LoadIndicator.SPINNER)))
    .addWidget(image(HOST_URL + 'images/gmail_add_on/business-sales.png', 'Send personalized videos and track them').setOnClickAction(action('savePlanType', {plan_type: '4', individual_segment: 'Sales'}, CardService.LoadIndicator.SPINNER)))
    .addWidget(image(HOST_URL + 'images/gmail_add_on/others.png', 'Others').setOnClickAction(action('showOthersScreen', {plan_type: '4'}, CardService.LoadIndicator.SPINNER))));
    // .addWidget(image(HOST_URL + 'images/gmail_add_on/others.png', 'Others').setOnClickAction(action('showOthersScreen', {plan_type: '4'}, CardService.LoadIndicator.SPINNER)))).build();
    // return CardService.newActionResponseBuilder().setNavigation(CardService.newNavigation().pushCard(card)).build();
    return card;
}

function showPersonalIndividualSegmants() {
    var card = CardService.newCardBuilder().setName('PersonalIndividualSegmentCard')
    .addSection(CardService.newCardSection()
    .addWidget(image(HOST_URL + 'images/gmail_add_on/plan-type-selection.jpg', ''))
    .addWidget(image(HOST_URL + 'images/gmail_add_on/business-ind-seg-text.jpg', ''))
    .addWidget(image(HOST_URL + 'images/gmail_add_on/personal-freelancer.jpg', 'Freelancer').setOnClickAction(action('savePlanType', {plan_type: '1', individual_segment: 'Freelancer'}, CardService.LoadIndicator.SPINNER)))
    .addWidget(image(HOST_URL + 'images/gmail_add_on/personal-youtuber.jpg', 'Youtuber').setOnClickAction(action('savePlanType', {plan_type: '1', individual_segment: 'Youtuber'}, CardService.LoadIndicator.SPINNER)))
    .addWidget(image(HOST_URL + 'images/gmail_add_on/personal-gamer.jpg', 'Gamer').setOnClickAction(action('savePlanType', {plan_type: '1', individual_segment: 'Gamer'}, CardService.LoadIndicator.SPINNER)))
    .addWidget(image(HOST_URL + 'images/gmail_add_on/others.png', 'Others').setOnClickAction(action('showOthersScreen', {plan_type: '1'}, CardService.LoadIndicator.SPINNER)))).build();
    return CardService.newActionResponseBuilder().setNavigation(CardService.newNavigation().pushCard(card)).build();
}

function showOthersScreen(event) {
    var params = event.parameters;
    var submitImage = params.plan_type == '4' ? 'images/gmail-gs/next.jpg' : 'images/gmail_add_on/getting-started.png';
    var hint = params.plan_type == '4' ? 'e.g. Support' : 'e.g. Blogger';
    var card = CardService.newCardBuilder().setName('PlanTypeOthersCard')
    .addSection(CardService.newCardSection()
    .addWidget(CardService.newTextInput().setFieldName('plan_type_others_input').setHint(hint).setMultiline(false).setTitle('Segment').setValue(''))
    .addWidget(image(HOST_URL + submitImage).setOnClickAction(action('validateAndSavePlanType', {plan_type: params.plan_type}, CardService.LoadIndicator.SPINNER)))).build();
    return CardService.newActionResponseBuilder().setNavigation(CardService.newNavigation().pushCard(card)).build();
}

function validateAndSavePlanType(event) {
    var formInput = event.formInput;
    var othersValue = formInput ? (formInput.plan_type_others_input ? formInput.plan_type_others_input : '') : '';
    var params = event.parameters;
    if(othersValue.trim() !== '') {
        event.parameters.individual_segment = othersValue;
        return savePlanType(event);
    } else {
        var submitImage = params.plan_type == '4' ? 'images/gmail-gs/next.jpg' : 'images/gmail_add_on/getting-started.png';
        var hint = params.plan_type == '4' ? 'e.g. Support' : 'e.g. Blogger';
        var card = CardService.newCardBuilder().setName('PlanTypeOthersCard')
        .addSection(CardService.newCardSection()
        .addWidget(textParagraph('<font color="#FF0000">User segment cannot be empty</font>'))
        .addWidget(CardService.newTextInput().setFieldName('plan_type_others_input').setHint(hint).setMultiline(false).setTitle('Segment').setValue(''))
        .addWidget(image(HOST_URL + submitImage).setOnClickAction(action('validateAndSavePlanType', {plan_type: params.plan_type}, CardService.LoadIndicator.SPINNER))));
        return CardService.newActionResponseBuilder().setNavigation(CardService.newNavigation().updateCard(card.build())).build();
    }   
}

function savePlanType(event) {
    logToGasLogs('savePlanType', 'entered save plan type method');
    var plan_type = event.parameters.plan_type;
    var individual_segment = event.parameters.individual_segment;
    var email = Session.getEffectiveUser().getEmail();
    var userProperties = PropertiesService.getUserProperties();
    var hippoAuthToken = userProperties.getProperty(HOST + AUTH_TOKEN_CACHE_KEY);
    var url = HOST_URL + 'wiz_accounts/plan_type/'+plan_type;
    url += "?email=" + email + '&authentication_token=' + hippoAuthToken;
    var headers = {'Accept': 'application/json'};
    var data = { 'request_type': 'api', 'referer': 'gmail-add-on', 'user_referral': 'gmail-add-on', 'plan_type_txt': individual_segment, 'sub_category': [individual_segment]};
    var options = { 'headers': headers, 'method': 'post', 'payload': JSON.stringify(data), 'muteHttpExceptions': true};
    var response = UrlFetchApp.fetch(url, options);
    var code = response.getResponseCode();
    if (code >= 200 && code < 300) {
        logToGasLogs('savePlanType', 'plan type updated successfully');
        userProperties.setProperty(HOST + SHOW_PLAN_TYPE, 'false');
        if (plan_type === '1') {
            var takeTour = takeTour();
            return CardService.newActionResponseBuilder().setNavigation(CardService.newNavigation().popToRoot().updateCard(takeTour)).build();
        } else if (plan_type == '4') {
            logToGasLogs('savePlanType', 'domain form called');
            var domainForm =  showDomainForm({}).build();
            return CardService.newActionResponseBuilder().setNavigation(CardService.newNavigation().popToRoot().updateCard(domainForm)).build();
        }
    }
}

function showDomainForm(event) {
    var params = event.parameters || {};
    var section = CardService.newCardSection();
    section.addWidget(image(HOST_URL + 'images/gmail_add_on/domain-form-image.jpg', ''));
    var firstName = params.first_name || '';
    var companyName = params.company_name || '';
    var phoneNumber = params.phone_number || '';
    section.addWidget(CardService.newTextInput().setTitle('First Name*').setFieldName('first_name').setHint('Your First Name').setMultiline(false).setValue(firstName));
    section.addWidget(CardService.newTextInput().setTitle('Company Name*').setFieldName('company_name').setHint('Your Company Name').setMultiline(false).setValue(companyName));
    section.addWidget(CardService.newTextInput().setTitle('Phone*').setFieldName('phone').setHint('Your Phone Number').setMultiline(false).setValue(phoneNumber));
    section.addWidget(image(HOST_URL + 'images/gmail_add_on/getting-started.png', 'Get Started').setOnClickAction(action('validateDomainForm', {}, CardService.LoadIndicator.SPINNER)));
    if (params.error) {
        section.addWidget(textParagraph('<font color="#FF0000">' + params.error + '</font>'));
    }
    var card = CardService.newCardBuilder().setName('DomainForm').addSection(section);
    if(params.error) {
        return CardService.newActionResponseBuilder().setNavigation(CardService.newNavigation().updateCard(card.build()));
    } else {
        return card;
    }
}

function validateDomainForm(event) {
    var formInput = event.formInput;
    var firstName = formInput ? (formInput.first_name ? formInput.first_name.trim() : '') : '';
    var companyName = formInput ? (formInput.company_name ? formInput.company_name.trim() : '') : '';
    var phoneNumber = formInput ? (formInput.phone ? formInput.phone.trim() : '') : '';
    if(firstName === '' || companyName === '' || phoneNumber === '') {
        logToGasLogs('validateDomainForm', 'domain form invalid');
        var errorMsg = 'Please enter all the values';
        if (firstName === '' && companyName !== '' && phoneNumber !== '') {
            errorMsg = 'First Name cannot be empty';
        } else if (firstName !== '' && companyName === '' && phoneNumber !== '') {
            errorMsg = 'Company Name cannot be empty';
        } else if (firstName !== '' && companyName !== '' && phoneNumber === '') {
            errorMsg = 'Phone Number cannot be empty';
        }
        return showDomainForm({parameters: {error: errorMsg, first_name: firstName, company_name: companyName, phone_number: phoneNumber}}).build();
    } else {
        logToGasLogs('validateDomainForm', 'domain form valid');
        var email = Session.getEffectiveUser().getEmail();
        var userProperties = PropertiesService.getUserProperties();
        var hippoAuthToken = userProperties.getProperty(HOST + AUTH_TOKEN_CACHE_KEY);
        var url = HOST_URL + 'user/save_branding_info';
        url += "?email=" + email + '&authentication_token=' + hippoAuthToken;
        var headers = {'Accept': 'application/json'};
        var data = {request_type: 'api', company_name: companyName, phone: phoneNumber, first_name: firstName};
        var options = {'headers': headers,'method': 'post', 'payload': data, 'muteHttpExceptions': true};
        var response = UrlFetchApp.fetch(url, options);
        var code = response.getResponseCode();
        if (code >= 200 && code < 300) {
            logToGasLogs('validateDomainForm', 'domain form submitted successfully');
            var respString = response.getContentText();
            var responseJson = JSON.parse(respString);
            if (responseJson.status) {
                userProperties.setProperty(HOST + ASK_COMPANY_DETAILS, 'false');
                return takeTour();
            } else {
                return showDomainForm({ parameters: { error: responseJson.msg, first_name: firstName, company_name: companyName, phone_number: phoneNumber } }).build();
            }
        } 
    }
}

//builders
function textButton(text) {
    return CardService.newTextButton().setText(text);
}

function iconAndText(iconUrl, content) {
    return CardService.newKeyValue().setIconUrl(iconUrl).setContent(content);  
}

function keyValue(topLabel, content) {
    return CardService.newKeyValue().setTopLabel(topLabel).setContent(content);
}

function iconTextAndBottomLabel(iconUrl, content, bottomLabel) {
      return CardService.newKeyValue().setIconUrl(iconUrl).setContent(content).setMultiline(true).setBottomLabel(bottomLabel);
}

function imageButton(url, altText, icon) {
    var imageCard = CardService.newImageButton();
    if(url) {
        imageCard.setIconUrl(url);
    } 
    if(altText) {
        imageCard.setAltText(altText);
    }
    if(icon) {
        imageCard.setIcon(icon);
    }
    return imageCard;
}

function authorizationAction(authorize_url) {
    return CardService.newAuthorizationAction().setAuthorizationUrl(authorize_url);
}

function action(function_name, parameters, load_indicator) {
    var actionCard = CardService.newAction();
    if(function_name) {
        actionCard.setFunctionName(function_name);
    }
    if(parameters) {
        actionCard.setParameters(parameters);
    }
    var indicator = load_indicator || CardService.LoadIndicator.NONE; 
    actionCard.setLoadIndicator(indicator);
    return actionCard;
}

function openLink(url, openAs, onClose) {
    var openLinkCard = CardService.newOpenLink().setUrl(url);
    if(openAs) {
       openLinkCard.setOpenAs(openAs);
    }
    if(onClose) {
       openLinkCard.setOnClose(onClose);
    }
    return openLinkCard;
}

function header(title, subtitle, imageUrl, imageStyle, imageAltText) {
    var headerCard = CardService.newCardHeader();
    if(title) {
        headerCard.setTitle(title);
    }
    if(subtitle) {
        headerCard.setSubtitle(subtitle);
    }
    if(imageUrl) {
        headerCard.setImageUrl(imageUrl);
    }
    if(imageStyle) {
        headerCard.setImageStyle(imageStyle);
    }
    if(imageAltText) {
        headerCard.setImageAltText(imageAltText);
    }
    return headerCard;
}

function textParagraph(text) {
    return CardService.newTextParagraph().setText(text);
}

function image(imageUrl, altText) {
    return CardService.newImage().setAltText(altText).setImageUrl(imageUrl);
}

function logToGasLogs(method, msg) {
    try {
         console.log(Session.getEffectiveUser().getEmail() + ' :: ' + method + ' :: ' + msg);
    } catch(exception) {}
}

        